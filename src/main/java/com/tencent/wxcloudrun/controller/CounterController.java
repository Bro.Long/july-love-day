package com.tencent.wxcloudrun.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencent.wxcloudrun.config.ApiResponse;
import com.tencent.wxcloudrun.dto.CounterRequest;
import com.tencent.wxcloudrun.model.Counter;
import com.tencent.wxcloudrun.service.CounterService;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * counter控制器
 */
@RestController

public class CounterController {

  final CounterService counterService;
  final Logger logger;

  public CounterController(@Autowired CounterService counterService) {
    this.counterService = counterService;
    this.logger = LoggerFactory.getLogger(CounterController.class);
  }


  /**
   * 获取当前计数
   * @return API response json
   */
  @GetMapping(value = "/api/count")
  ApiResponse get() {
    logger.info("/api/count get request");
    Optional<Counter> counter = counterService.getCounter(1);
    Integer count = 0;
    if (counter.isPresent()) {
      count = counter.get().getCount();
    }

    return ApiResponse.ok(count);
  }


  /**
   * 更新计数，自增或者清零
   * @param request {@link CounterRequest}
   * @return API response json
   */
  @PostMapping(value = "/api/count")
  ApiResponse create(@RequestBody CounterRequest request) {
    logger.info("/api/count post request, action: {}", request.getAction());

    Optional<Counter> curCounter = counterService.getCounter(1);
    if (request.getAction().equals("inc")) {
      Integer count = 1;
      if (curCounter.isPresent()) {
        count += curCounter.get().getCount();
      }
      Counter counter = new Counter();
      counter.setId(1);
      counter.setCount(count);
      counterService.upsertCount(counter);
      return ApiResponse.ok(count);
    } else if (request.getAction().equals("clear")) {
      if (!curCounter.isPresent()) {
        return ApiResponse.ok(0);
      }
      counterService.clearCount(1);
      return ApiResponse.ok(0);
    } else {
      return ApiResponse.error("参数action错误");
    }
  }


  @RequestMapping("/api/testopenid")
  public String getUserInfo(@RequestParam(name = "code") String code) throws Exception {
    System.out.println("code" + code);
    String url = "https://api.weixin.qq.com/sns/jscode2session";
    url += "?appid=wxf51d7915e0ee4277";//自己的appid
    url += "&secret=78b95a86824dd3c7b7ba95989a7ae05d";//自己的appSecret
    url += "&js_code=" + code;
    url += "&grant_type=authorization_code";
    url += "&connect_redirect=1";
    String res = null;
    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
    // DefaultHttpClient();
    HttpGet httpget = new HttpGet(url);    //GET方式
    CloseableHttpResponse response = null;
    // 配置信息
    RequestConfig requestConfig = RequestConfig.custom()          // 设置连接超时时间(单位毫秒)
            .setConnectTimeout(5000)                    // 设置请求超时时间(单位毫秒)
            .setConnectionRequestTimeout(5000)             // socket读写超时时间(单位毫秒)
            .setSocketTimeout(5000)                    // 设置是否允许重定向(默认为true)
            .setRedirectsEnabled(false).build();           // 将上面的配置信息 运用到这个Get请求里
    httpget.setConfig(requestConfig);                         // 由客户端执行(发送)Get请求
    response = httpClient.execute(httpget);                   // 从响应模型中获取响应实体
    HttpEntity responseEntity = response.getEntity();
    System.out.println("响应状态为:" + response.getStatusLine());
    if (responseEntity != null) {
      res = EntityUtils.toString(responseEntity);
      System.out.println("响应内容长度为:" + responseEntity.getContentLength());
      System.out.println("响应内容为:" + res);
    }
    // 释放资源
    if (httpClient != null) {
      httpClient.close();
    }
    if (response != null) {
      response.close();
    }
    JSONObject jo = JSON.parseObject(res);
    String openid = jo.getString("openid");
    System.out.println("openid" + openid);
    return openid;
  }
  
}